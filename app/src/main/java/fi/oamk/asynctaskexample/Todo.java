package fi.oamk.asynctaskexample;

import org.json.JSONObject;

public class Todo implements BaseAsyncTaskCallback {

    static final String URL = "http://10.0.2.2/asyncresttest/test";

    enum Action {
        GET,
        ADD,
        EDIT,
        DELETE
    }

    private TodoCallback mTodoCallBack;
    private Action action;

    public Todo(TodoCallback todoCallback) {
        mTodoCallBack = todoCallback;
    }

    public void get() {
        action = Action.GET;

        new BaseAsyncTask(this).execute(URL,"GET");
    }

    public void add(String title, String description) {
        JSONObject post_dict = new JSONObject();
        try {
            post_dict.put("title", title);
            post_dict.put("description", description);
            action = Action.ADD;
            new BaseAsyncTask(this).execute(URL,"POST",String.valueOf(post_dict));
        }
        catch (Exception ex) {
            mTodoCallBack.onError(ex);
        }

    }

    public void edit() {
        action = Action.EDIT;
    }

    public void delete() {
        action = Action.DELETE;
    }

    @Override
    public void onSuccess(String result) {
        switch (action) {
            case GET:
                mTodoCallBack.onGet(result);
                break;
            case ADD:
                mTodoCallBack.onAdd();
                break;
            case EDIT:
                mTodoCallBack.onEdit();
                break;
            case DELETE:
                mTodoCallBack.onDelete();
                break;
            default:
                mTodoCallBack.onError(new Exception("Unknown action in todo."));
        }
    }

    @Override
    public void onFailure(Exception ex) {
        mTodoCallBack.onError(ex);
    }
}
