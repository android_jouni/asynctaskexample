package fi.oamk.asynctaskexample;

public interface TodoCallback {
    void onGet(String string);
    void onAdd();
    void onEdit();
    void onDelete();
    void onError(Exception ex);
}
