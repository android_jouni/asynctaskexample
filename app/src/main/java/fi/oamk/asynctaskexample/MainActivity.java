package fi.oamk.asynctaskexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements TodoCallback {

    private TextView tvTest;
    private Todo todo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        todo = new Todo(this);
        tvTest = findViewById(R.id.tvTest);

        todo.get();
    }

    @Override
    public void onGet(String string) {

        tvTest.setText(string);
    }

    @Override
    public void onAdd() {
        todo.get();
    }

    @Override
    public void onEdit() {

    }

    @Override
    public void onDelete() {

    }

    @Override
    public void onError(Exception ex) {
        tvTest.setText(ex.getMessage());
    }

    public void testAdd(View view) {

        todo.add("test 9","Test message from Android");
    }
}
