package fi.oamk.asynctaskexample;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class BaseAsyncTask extends AsyncTask<String,String,String> {
    
    static final int CONNECTION_TIMEOUT = 5000;

    private Exception mException;
    private BaseAsyncTaskCallback mBaseAsyncTaskCallback;


    public BaseAsyncTask(BaseAsyncTaskCallback baseAsyncTaskCallback) {
        mBaseAsyncTaskCallback = baseAsyncTaskCallback;
    }

    @Override
    protected String doInBackground(String ... strings) {
        HttpURLConnection urlConnection = null;
        String result = "";
        String address = strings[0];
        String method = strings[1];

        try {
            urlConnection = openHttpConnection(address, method);

            if (method == "POST" || method== "PUT") {
                writeToOutputStream(strings[2],urlConnection.getOutputStream());
            }
            else if (method == "DELETE") {

            }


            int responseCode = urlConnection.getResponseCode();
            
            if (responseCode == 200 || responseCode == 201) {
                if (method == "GET") {
                    result = inputStreamToString(urlConnection.getInputStream());
                }
            }
            else {
                mException = new Exception("Error executing HTTP request. HTTP response is " + responseCode);
            }
        }
        catch (Exception ex) {
            mException = ex;
        }
        finally {
             if (urlConnection != null) {
                 urlConnection.disconnect();
             }
        }
        return result;
    }


    private HttpURLConnection openHttpConnection(String address, String method) throws Exception {
        HttpURLConnection result = null;
        URL url = new URL(address);
        result = (HttpURLConnection)url.openConnection();
        result.setConnectTimeout(CONNECTION_TIMEOUT);
        result.setReadTimeout(CONNECTION_TIMEOUT);
        result.setRequestMethod(method);
        return result;
    }

    private void writeToOutputStream(String json, OutputStream os) throws Exception {
        os.write(json.getBytes("UTF-8"));
        os.close();
    }


    // Converts stream retrieved through HTTP to string.
    private String inputStreamToString(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();

        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line;

            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                }
                catch (Exception ex) {
                    mException = new Exception(ex.getMessage());
                }
            }
        }
        return response.toString();
    }




    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (mBaseAsyncTaskCallback != null) {
            if (mException == null) {
                mBaseAsyncTaskCallback.onSuccess(result);
            }
            else {
                mBaseAsyncTaskCallback.onFailure(mException);
            }
        }
    }
}
