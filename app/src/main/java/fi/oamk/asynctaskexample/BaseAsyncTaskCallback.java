package fi.oamk.asynctaskexample;

public interface BaseAsyncTaskCallback{
    void onSuccess(String result);
    void onFailure(Exception ex);
}
